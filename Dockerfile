FROM openjdk:8-jdk-alpine as builder

COPY . /home/gradle/src
WORKDIR /home/gradle/src
RUN ./gradlew assemble

FROM openjdk:8-jdk-alpine
COPY --from=builder /home/gradle/src/build/libs/app.jar /app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]