package com.kfirfer.samples.testcontainers.controller;

import com.kfirfer.samples.testcontainers.model.Customer;
import com.kfirfer.samples.testcontainers.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    CustomerRepository repository;

    @PostMapping
    public Customer add(@RequestBody Customer customer) {
        return repository.save(customer);
    }

    @GetMapping("/{id}")
    public Customer findById(@PathVariable("id") Long id) {
        Optional<Customer> optCustomer = repository.findById(id);
        if (optCustomer.isPresent())
            return optCustomer.get();
        else
            return null;
    }

}