package com.kfirfer.samples.testcontainers;

import com.kfirfer.samples.testcontainers.model.Transaction;
import com.kfirfer.samples.testcontainers.repository.TransactionRepository;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RedisTransactionRepositoryTest extends AbstractIntegrationTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void testAdd() {
        Transaction transaction = new Transaction(1L, 1000, new Date(), 20L, 40L);
        transaction = transactionRepository.save(transaction);
        Assert.assertNotNull(transaction);
    }

    @Test
    public void testFindByFromAccountId() {
        List<Transaction> transactions = transactionRepository.findByFromAccountId(20L);
        Assert.assertEquals(1, transactions.size());
    }

    @Test
    public void testFindByToAccountId() {
        List<Transaction> transactions = transactionRepository.findByToAccountId(40L);
        Assert.assertEquals(1, transactions.size());
    }
}
